package View;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

public class MainView {

	private JFrame frame;
	private JTable table;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainView window = new MainView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Abrir Ventana secundaria");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VentanaSecundaria ventana = new VentanaSecundaria();
				ventana.frame.setVisible(true);
			}
		});
		btnNewButton.setBounds(213, 193, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		
		JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 11, 400, 250);
        frame.getContentPane().add(scrollPane);

        table = new JTable();
        scrollPane.setViewportView(table);

        // Se crea el modelo de la tabla
        DefaultTableModel modelo = new DefaultTableModel();

        // Se crean las columnas
        modelo.addColumn("DNI");
        modelo.addColumn("Nombre");
        modelo.addColumn("Domicilio");

        // Se crean las filas
        modelo.addRow(new String[] {"122345678","Jose","Tribulato 123"});
        modelo.addRow(new String[] {"122345678","Blanca","Independencia 420"});
        modelo.addRow(new String[] {"87654321","Pedro","SinSalida 69"});

        // Al final se incorpora el modelo a la tabla
        table.setModel(modelo);

        TableModel modelo1 = table.getModel();
        String s = (String)modelo1.getValueAt(2,2); // fila y columna
        modelo1.setValueAt("Nuevo dato", 1, 1);
        System.out.println(s);

        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment( JLabel.CENTER );
        dtcr.setBackground(Color.RED);
        dtcr.setForeground(Color.WHITE);

        TableColumn col = table.getColumnModel().getColumn(0);
        col.setCellRenderer(dtcr);

		
	}
}
