package Models;

public class Persona {
	
	//Clase persona son los nodos del grafo
	
	private String nombre;
	private Interes[] intereses;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Interes[] getIntereses() {
		return intereses;
	}
	public void setIntereses(Interes[] intereses) {
		this.intereses = intereses;
	}
	
	

}
